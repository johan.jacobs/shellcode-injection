CFLAGS=-fno-stack-protector -z execstack -ggdb3
LDFLAGS=
CC=gcc
SRC=src/unsafe.c
TARGET=unsafe

all: $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(TARGET) $(LDFLAGS)

getenvaddr: src/getenvaddr.c
	$(CC) $^ -o $@

clean:
	rm -f $(TARGET)
