/**
 * Source: Hacking: The Art of Exploitation, 2nd Edition, Jon Erickson
 * Chapter 0x330: Experimenting with BASH
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char *ptr = NULL;

	if(argc < 3) {
		printf("Usage: %s <environment variable> <target program name>\n", argv[0]);
		exit(0);
	}
	ptr = getenv(argv[1]); /* Get env var location */
	ptr += (strlen(argv[0]) - strlen(argv[2]))*2; /* Adjust for program name */
	printf("%s will be at %p\n", argv[1], ptr);
}
