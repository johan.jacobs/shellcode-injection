#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void copy(char* str) {
    char buffer[12];
    strcpy(buffer, str);
}

int main(int argc, char *argv[]) {
    if(argc < 2) {
        printf("Please provide an input string, argc = %d\n", argc);
        return 0;
    }

    copy(argv[1]);
    return 0;
}
